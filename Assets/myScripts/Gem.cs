﻿using UnityEngine;
using System.Collections;
using System; //This allows the IComparable Interface

public class Gem : IComparable<Gem>{
	//https://unity3d.com/learn/tutorials/modules/intermediate/scripting/lists-and-dictionaries
	public Color color;
	public string tag, id;
	public int power, col, row;
	public GameObject gemTarget;
	public bool matched,fall = false;
	
	public Gem(GameObject gemInstance, string newColor, int power, int newRow, int newCol){
		gemInstance.GetComponent<Rigidbody>().useGravity = false;
		gemInstance.GetComponent<Rigidbody>().GetComponent<Collider>().enabled = false;
		gemInstance.GetComponent<Renderer>().material.color = switchColor(newColor);
		tag = newColor;
		gemTarget = gemInstance;
		col = newCol;
		row = newRow;
		id = "col:" + col + "row:" + row;	
	}
	
	private Color switchColor(string newColor){
		Color materialColor;
		//newColor = "red";
		switch(newColor){
		case "red":
			//materialColor =  Color.red;
			//new Color(R,G,B,alpha);	
			materialColor = new Color(255F, 0F, 0F, 100F);
			break;
		case "green":
			//materialColor =  Color.green;
			materialColor = new Color(0F, 255F, 0F, 100F);
			break;
		case "blue":
			//materialColor =  Color.blue;
			materialColor = new Color(0F, 0F, 100F, 100F);
			break;
		case "yellow":
			//materialColor =  Color.yellow;
			materialColor = new Color(255F, 255F, 0F, 100F);
			break;
		case "cyan":
			//materialColor =  Color.cyan;
			materialColor = new Color(0F, 255F, 255F, 100F);
			break;
		case "magenta":
			//materialColor =  Color.magenta;
			materialColor = new Color(255F, 0F, 255F, 100F);
			break;
		default:
			materialColor = new Color(255F, 255F, 255F, 100F);
			break;
		}
		return materialColor;
		
	}
	public int CompareTo(Gem other){
		if(other == null)
		{
			return 1;
		}
		
		//Return the difference in power.
		return power - other.power;
	}
	
	/*Color color = new Color(Random.value, Random.value, Random.value);*/
}
