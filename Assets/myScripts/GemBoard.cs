﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GemBoard : MonoBehaviour {
	public int MAX_COLS, MAX_ROWS;
	public GameObject gemPrefab;
	private GameObject fromGem, toGem;
	private float distanceX = -3;
	private float distanceZ = -3;
	private float targetSpeed = 1;
	private string[] colors = new string[] { "red","green","blue","yellow","cyan","magenta" };
	private List<Gem> gems = new List<Gem> ();
	private Vector3 path;
    
	void Start () {	
		for (int i = 1; i < MAX_ROWS + 1; i++){
			for (int j = 1; j < MAX_COLS + 1; j++){	
				putGemAtRowCol(i, j);				
			}
		}
	}
    
	private void putGemAtRowCol(int row, int col ){
		//FIXME: nao funciona - gemInstance.collider.isTrigger = false;
		int MyIndex = Random.Range(0,(colors.Length));
		string color = colors[MyIndex];

		GameObject gemInstance = Instantiate (gemPrefab, new Vector3(row * distanceX, 0, col * distanceZ), Quaternion.identity) as GameObject;
		gems.Add( new Gem(gemInstance, color, 50, row, col));
	}
    
	void MoveGameObject(GameObject obj, Vector3 from, Vector3 to, int y){
		Vector3[] v = new Vector3[3];
		v[0] = from;
		v[1] = new Vector3((to.x+from.x)/2,(to.y+from.y)/2,(to.z+from.z)/2);
		v[1].y = y;
		v[2] = to; 
		Hashtable ht = new Hashtable(); 
		ht.Add("path", v); 
		ht.Add("time", targetSpeed); 
		//FIXME nao funciona ht.Add("oncomplete", "enableMove"); 
		ht.Add("oncomplete", "enableMove"); 
		ht.Add("transition", "easeInOutSine"); 
		iTween.MoveTo(obj,ht);   
	}
    
	/*private void enableMove(){
		Debug.Log ("testes");
	}*/
    
	/*private ArrayList invertPositions(Gem sourceGem,Gem destinGem){
		string idTemp = sourceGem.id;
		int colTemp = sourceGem.col;
		int rowTemp = sourceGem.row;
		//INFO: CUIDADO com valores (source, destino row e destino col)
		searchForTriplets(sourceGem, destinGem.row, destinGem.col);
		sourceGem.col = destinGem.col;
		sourceGem.row = destinGem.row;
		sourceGem.id  = destinGem.id;
		destinGem.col = colTemp;
		destinGem.row = rowTemp;
		destinGem.id  = idTemp;

		ArrayList returns = new ArrayList();
		returns.Add (destinGem);
		returns.Add (sourceGem);

		return returns;
	}
    */
	private void gemSwiped(){ // From Gem e To Gem
		//var dir = clique
		Gem sourceGem = getGemProperties(fromGem);
		Gem destinGem = getGemProperties(toGem);

		if (sourceGem is Gem && destinGem is Gem && destinGem != sourceGem) {
			searchForTriplets(sourceGem, destinGem.row, destinGem.col);
			//INFO: INVERTE id, coluna e linha das joias*********************
			string idTemp = sourceGem.id;
			int colTemp = sourceGem.col;
			int rowTemp = sourceGem.row;
			sourceGem.col = destinGem.col;
			sourceGem.row = destinGem.row;
			sourceGem.id  = destinGem.id;
			destinGem.col = colTemp;
			destinGem.row = rowTemp;
			destinGem.id  = idTemp;
			// INVERTE id, coluna e linha das joias*********************
			MoveGameObject (fromGem, sourceGem.gemTarget.transform.position, destinGem.gemTarget.transform.position, -3);
			MoveGameObject (toGem, destinGem.gemTarget.transform.position, sourceGem.gemTarget.transform.position, 3);

		}
		gemsFillGaps();
		dropNewGems();
	}
    
    private void gemDroped(){ // From Gem e To Gem
		Gem sourceGem = getGemProperties(fromGem);
		Gem destinGem = getGemProperties(toGem);

		if (sourceGem is Gem && destinGem is Gem && destinGem != sourceGem) {
			
			//INFO: INVERTE id, coluna e linha das joias*********************
			string idTemp = sourceGem.id;
			int colTemp = sourceGem.col;
			int rowTemp = sourceGem.row;
			/*sourceGem.col = destinGem.col;
			sourceGem.row = destinGem.row;
			sourceGem.id  = destinGem.id;*/
			destinGem.col = colTemp;
			destinGem.row = rowTemp;
			destinGem.id  = idTemp;
			// INVERTE id, coluna e linha das joias*********************
			MoveGameObject (fromGem, sourceGem.gemTarget.transform.position, destinGem.gemTarget.transform.position, 0);
			//MoveGameObject (toGem, destinGem.gemTarget.transform.position, sourceGem.gemTarget.transform.position, 0);
		}
	}
    
	private void dropNewGems(){
		for (int i = 1; i < MAX_ROWS + 1; i++) {
			for (int j = 1; j < MAX_COLS + 1; j++) {	
				Gem gem = getGemAtRowCol (i, j);
				if(gem.fall){
					fromGem = gem.gemTarget;	
					Gem nextGem = getGemAtRowCol (i + 1, j);
					gem.fall = false;
					nextGem.fall = false;
					if(nextGem is Gem){
						toGem = nextGem.gemTarget;
                        //TODO: Cair joia
						gemDroped();
					}else{
						//TODO Criar joia
                        putGemAtRowCol(i, j);
					}
				}
			}
		}
	}
    
	private void gemsFillGaps(){
		foreach(Gem joia in gems){
			if (joia.matched == true){				
				//INFO Destroi a joia e anula a propriedade gemTarget
				//Destroy(joia.gemTarget);
				joia.gemTarget = null;
				//joia.gemTarget.renderer.material.color = Color.black;
				//gems.Remove(joia); remover objeto da classe
			}
		}
		for (int i = 1; i < MAX_ROWS + 1; i++){
			for (int j = 1; j < MAX_COLS + 1; j++){	
				Gem gem = getGemAtRowCol(i, j);
				//if (gem.gemTarget.renderer.material.color == Color.black){
                
                if (gem.gemTarget == null){
					//TODO: procurar pelas joias em volta
					searchInDirections(gem, i, j,"vertical", -1,"fall");
					//TODO: cair novas joias
					//putGemAtRowCol(i, j);	
				}						
			}
		}
	}
    
	private void updateGemPosition(Gem sourceGem,Gem destinGem){

	}
    
	private void searchForTriplets(Gem gem, int row, int col){	
		//pesquisa horizontal

		searchInDirections(gem, row, col,"horizontal",-1,"match");
		searchInDirections(gem, row, col,"horizontal", 1,"match");
		searchInDirections(gem, row, col,"vertical", -1,"match");
		searchInDirections(gem, row, col,"vertical", 1,"match");
	}
    
	private Gem searchInDirections(Gem gem, int row, int col,string direction, int value, string action){
		int index = 1 * value;
		bool matched = true;
		Gem nextGem;
		/*do
		{*/
			nextGem = getNextGem(row, col, direction,index);			
			if(nextGem is Gem){
				if(action == "match"){
					matched = verifyMatch (gem, nextGem);
				}else{
					matched = markFall (nextGem);
				}
			}else{
				matched = true;
			}
			index ++;
			
		/*} while(matched == false);*/

		return nextGem;
	}
    
	private bool verifyMatch(Gem gem, Gem nextGem){
		if (gem.tag == nextGem.tag) {
			//nextGem.gemTarget.renderer.material.color = Color.black;
		//	Debug.Log("JOIAS COM MESMA TAG : "+ gem.tag +  nextGem.tag );
			gem.matched = true;
			nextGem.matched = true;		
			return true;
		} else {
			return false;
		}
	}

	private bool markFall(Gem nextGem){
		//Debug.Log (nextGem.id);
		nextGem.gemTarget.GetComponent<Renderer>().material.color = Color.white;
		nextGem.fall = true;
		return false;
	}

	private Gem getNextGem(int row, int col, string direction, int value){
		Gem nextGem;
		switch(direction){
		case "vertical":
			nextGem = getGemAtRowCol (row + value, col);
			break;
		case "horizontal":
			nextGem = getGemAtRowCol (row, col + value);
			break;
		case "diagonal":
			nextGem = getGemAtRowCol (row + value, col + value);
			break;
		default:
			nextGem = getGemAtRowCol (row , col+ value);
			break;
		}
		return nextGem;	
	}
	private void MoveObject (GameObject thisTransform, Vector3 startPos, Vector3 endPos, float time) {
		Vector3 currentPosition = Vector3.Lerp (startPos, endPos, time);	
		thisTransform.transform.position = currentPosition;
		
	}

	private Gem getGemProperties(GameObject target){
		Gem gem = gems.Find (myItem => myItem.gemTarget == target);
		//Destroy (gem);

		return gem;
	}

	private Gem getGemAtRowCol(int row, int col){
		var gem = gems.Find (myItem => myItem.id == "col:"+col+"row:"+row);

		return gem;

	}

	// Update is called once per frame
	void Update () {
		avoidBreak ();
		if( Input.GetMouseButtonDown(0) )
		{
			Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit hit;
			
			if( Physics.Raycast( ray, out hit, 100 ) )
			{
				fromGem = hit.transform.gameObject;

				/*Gem joia = getGemProperties(fromGem);
				Debug.Log (joia.col + " " + joia.row);
				Debug.Log (joia.id);*/
			}
		}
		if( Input.GetMouseButtonUp(0) )
		{
			Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit hit;
			
			if( Physics.Raycast( ray, out hit, 100 ) )
			{	
				toGem = hit.transform.gameObject;
				gemSwiped();
				//Debug.Log( hit.transform.gameObject.name );
			}
		}
	}
	private void avoidBreak(){
		var fps = 1.0f / Time.deltaTime;
		if(fps < 5) {
			//Debug.Break();
		}
	}
	/*private void hasAnyMatch(){
		//return Boolean;
	}
	
	private void swapAndSearch(){
		
	}

	private void moveGemToLocation(GameObject gemPrefab,int row,int col, bool userInited){
		
	}
	
	private void locationForRowCol(int row, int col){
		//return Point;
	}
	

	

	

	
	private void removeGemFromRowCol(int row,int col){
		
	}*/
	/*private void putGemAtRowCol( int row, int col, int fromrow , int fromcol )
	{
		
	}

	private void makeLevelHaveNoMatch (){
		
	}
	private void gemMoved(Event e){
		//searchForTriplets();
	}*/

	

}
//http://docs.unity3d.com/Documentation/ScriptReference/Random.Range.html
//https://unity3d.com/learn/tutorials/modules/beginner/scripting/loops
//https://unity3d.com/learn/tutorials/modules/beginner/scripting/instantiate
//https://unity3d.com/learn/tutorials/modules/intermediate/scripting/lists-and-dictionaries
//http://answers.unity3d.com/questions/12785/is-there-a-way-to-define-arrays-and-objects-litera.html
//foreach(float gem in GemArray){
//private Dictionary<string, Color> colors = new Dictionary<string, Color>();
/*OLD IN update()
 * /*if (Input.GetButtonDown ("Fire1")) {
		Debug.Log ('O');		
	}
	if (Input.GetButtonUp ("Fire1")) {
		Debug.Log ('A');		
	}*/

/*OLD putGemAtRowCol()
 * /*foreach(Gem joia in gems)
        {
            print (joia.row + " " + joia.col);
        }*/

//Destroy( teste );
/*GameObject[] objects = GameObject.FindGameObjectsWithTag( "red" );
		foreach( GameObject go in objects )
		{
			Destroy( go );
		}*/


/* OLD IN GEMSWIPED
iTween.MoveTo(fromGem, iTween.Hash("path" , iTweenPath.GetPath("PathGem"), "time", targetSpeed));
*/
//toGem.transform.forward = Vector3.SmoothDamp(toGem.transform.forward, fromGemPos, 17, 1.0f, /*maxSpeed*/);

//fromGem.transform.position = Vector3.Lerp(fromGemPos, toGemPos, step);
//toGem.transform.position = Vector3.Lerp(toGemPos, fromGemPos, step);
/*MoveObject(fromGem, fromGemPos, toGemPos, step);
MoveObject(toGem, toGemPos, fromGemPos, step);*/

/*http://forum.unity3d.com/threads/56990-iTween-oncomplete-callback-is-not-working
http://forum.unity3d.com/threads/62448-iTween-ValueTo
http://answers.unity3d.com/questions/646760/c-list-find-index-of-gameobject.html
http://answers.unity3d.com/questions/23388/generic-way-to-get-properties-on-component.html*/